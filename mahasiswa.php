<?php
    $DB_NAME = "kampus";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";
    $data_mhs = array();
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT m.nim,m.nama,p.nama_prodi,concat('http://192.168.43.68/app-x0a-web/images/',photos) as url, m.alamat,j.kelamin
        FROM mahasiswa m,prodi p , jenis_kelamin j
        WHERE m.id_prodi=p.id_prodi AND m.id_jk = j.id_jk";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            while($mhs = mysqli_fetch_assoc($result)){
                array_push($data_mhs,$mhs);
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kampus</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Kampus</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link active" href="#">List Mahasiswa <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="about.php">About</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h4 class="mt-3 mb-3">Daftar Mahasiswa</h4>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">NIM</th>
        <th scope="col">Nama</th>
        <th scope="col">Prodi</th>
        <th scope="col">Foto</th>
        <th scope="col">Alamat</th>
        <th scope="col">Jenis Kelamin</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($data_mhs as $mhs) : ?>
        <tr>
            <td><?= $mhs['nim'] ?></td>
            <td><?= $mhs['nama'] ?></td>
            <td><?= $mhs['nama_prodi'] ?></td>
            <td><img src="<?= $mhs['url'] ?>" width="40px" height="50px" /></td>
            <td><?= $mhs['alamat'] ?></td>
            <td><?= $mhs['kelamin'] ?></td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>