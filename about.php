<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kampus</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Kampus</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="mahasiswa.php">List Mahasiswa</span></a>
                <a class="nav-item nav-link active" href="#">About <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
        <div class="card mt-3">
            <div class="card-header">
                Tentang
            </div>
            <div class="card-body">
                <h5 class="card-title">Sistem informasi Kampus</h5>
                Dibuat oleh :
                <ul>
                    <li>Agysta Wahyu Wicaksono - 1931733106</li>
                    <li>Pandu Virga Pradana - 1931733095</li>
                </ul>
                
            </div>
        </div>
    </div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
